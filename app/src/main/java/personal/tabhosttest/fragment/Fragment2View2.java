package personal.tabhosttest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import personal.tabhosttest.R;
import personal.tabhosttest.activity.BaseActivity;

/**
 * @author Hatem Toumi
 */
public class Fragment2View2 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment2_view2, container, false);

        Button next = (Button) v.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).performTransaction(new Fragment3View2(), null, true);
            }
        });
        return v;
    }
}
