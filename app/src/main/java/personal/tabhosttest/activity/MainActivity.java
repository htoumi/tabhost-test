package personal.tabhosttest.activity;

import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import personal.tabhosttest.R;

public class MainActivity extends TabActivity {
    private static final String SWITCH_TABS_RECEIVER = "personal.tabhosttest.SWITCH_TABS_RECEIVER";
    private static final String TAB_INDEX = "TAB_INDEX";

    private TabHost tabHost;
    private TabHost.TabSpec spec;

    private BroadcastReceiver switchTabsReceiver = new BroadcastReceiver() {
        @Override
        public synchronized void onReceive(Context context, Intent intent) {
            if (intent.getAction() == null) {
                return;
            }

            if (intent.getAction().equals(SWITCH_TABS_RECEIVER)) {
                int tabIndex = intent.getIntExtra(TAB_INDEX, -1);
                switch (tabIndex) {
                    case 0:
                        tabHost.setCurrentTab(0);
                        break;
                    case 1:
                        tabHost.setCurrentTab(1);
                        break;
                    case 2:
                        tabHost.setCurrentTab(2);
                        break;
                    default:
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        // Activity View 1
        spec = tabHost.newTabSpec("View 1")
                .setIndicator(createTabView(android.R.drawable.ic_menu_mapmode, "View 1", true))
                .setContent(new Intent(this.getApplicationContext(), ActivityView1.class));
        tabHost.addTab(spec);

        // Activity View 2
        spec = tabHost.newTabSpec("View 2")
                .setIndicator(createTabView(android.R.drawable.ic_menu_agenda, "View 2", true))
                .setContent(new Intent(this.getApplicationContext(), ActivityView2.class));
        tabHost.addTab(spec);

        // Activity View 3
        spec = tabHost.newTabSpec("View 3")
                .setIndicator(createTabView(android.R.drawable.ic_menu_add, "View 3", true))
                .setContent(new Intent(this.getApplicationContext(), ActivityView3.class));
        tabHost.addTab(spec);

        tabHost.setOnTabChangedListener(new OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                Log.i("TabChangeListener", tabId);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        // register switchTabsReceiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SWITCH_TABS_RECEIVER);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(switchTabsReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // unregister switchTabsReceiver
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(switchTabsReceiver);
    }

    private View createTabView(final int id, final String text, boolean showContactNumber) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabbaritem, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
        imageView.setImageDrawable(getResources().getDrawable(id));
        TextView textView = (TextView) view.findViewById(R.id.tab_text);
        textView.setText(text);

        int contactsSize = 5;
        LinearLayout tab_contact_number = (LinearLayout) view.findViewById(R.id.tab_contact_number);
        if (!showContactNumber || contactsSize == 0) {
            tab_contact_number.setVisibility(View.INVISIBLE);
        } else {
            TextView textView2 = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt);
            textView2.setText(String.valueOf(contactsSize));
        }


        return view;
    }

    public static void setCurrentTab(Context context, int index) {

        // Broadcast intent
        Intent intent = new Intent();
        intent.setAction(MainActivity.SWITCH_TABS_RECEIVER);
        intent.putExtra(MainActivity.TAB_INDEX, index);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
