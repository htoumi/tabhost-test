package personal.tabhosttest.activity;

import android.os.Bundle;

import personal.tabhosttest.fragment.Fragment1View2;

/**
 * @author Hatem Toumi
 */
public class ActivityView2 extends BaseActivity {

    @Override
    protected String getTag() {
        return ActivityView2.class.getSimpleName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        performTransaction(new Fragment1View2(), null, false);
    }

    @Override
    public void onBackPressed() {
        if (!isFragmentVisible(Fragment1View2.class)) {
            super.onBackPressed();
        } else {
            MainActivity.setCurrentTab(this, 0);
        }
    }
}
