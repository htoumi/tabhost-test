package personal.tabhosttest.activity;

import android.os.Bundle;

import personal.tabhosttest.fragment.Fragment1View1;

/**
 * @author Hatem Toumi
 */
public class ActivityView1 extends BaseActivity {

    @Override
    protected String getTag() {
        return ActivityView1.class.getSimpleName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        performTransaction(new Fragment1View1(), null, false);
    }

    @Override
    public void onBackPressed() {
        if (!isFragmentVisible(Fragment1View1.class)) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
