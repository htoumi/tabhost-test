package personal.tabhosttest.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import personal.tabhosttest.R;

/**
 * @author Hatem Toumi
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    protected abstract String getTag();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(getTag(), "onCreate");
        setContentView(R.layout.activity_base);

        fragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(getTag(), "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(getTag(), "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(getTag(), "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(getTag(), "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(getTag(), "onDestroy");
    }

    public void performTransaction(Fragment fragment, Bundle bundle, boolean keepInBackStack) {
        if (fragment == null) {
            Log.w(getTag(), "performTransaction, fragment is null");
            return;
        }

        if (fragment.isVisible()) {
            Log.w(getTag(), "performTransaction, fragment is already visible");
            return;
        }

        fragmentTransaction = fragmentManager.beginTransaction();
        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        // check if fragment exist in backstack
        if (fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName()) == null) {
            // put fragment on container
            fragmentTransaction.add(R.id.fragmentContainer, fragment, fragment.getClass().getSimpleName());
        }

        if (keepInBackStack) {
            // Add this transaction to the back stack
            fragmentTransaction.addToBackStack(null);
        }

        fragmentTransaction.commit();
    }

    public void removeAllFragments() {
        while (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();
        }
    }

    /**
     * check if fragment is visible in container layout
     *
     * @param fragmentClass
     * @return
     */
    public boolean isFragmentVisible(Class fragmentClass) {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragmentContainer);
        return currentFragment.getClass() == fragmentClass;
    }
}
