package personal.tabhosttest.activity;

import android.os.Bundle;

import personal.tabhosttest.fragment.Fragment1View3;

/**
 * @author Hatem Toumi
 */
public class ActivityView3 extends BaseActivity {

    @Override
    protected String getTag() {
        return ActivityView3.class.getSimpleName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        performTransaction(new Fragment1View3(), null, false);
    }

    @Override
    public void onBackPressed() {
        if (!isFragmentVisible(Fragment1View3.class)) {
            super.onBackPressed();
        } else {
            MainActivity.setCurrentTab(this, 0);
        }
    }
}
